const enum Mode {
  Op = "@",
  Special = "!",
  None = " ",
}

interface User {
  nick: string;
  mode: Mode;
}

const Server: User = { nick: "Server", mode: Mode.Special };

const users = new Map<string, User>();

const shift = (x: number) => 0 | (x = (x ^= x << 13) ^ x >>> 17) ^ x << 5;
const hash = (name: string) => {
  const bytes = [...name].map(c => c.charCodeAt(0));
  return bytes.reduce((h, c) => ((h * 33) + c) | 0, 4);
};
const color = (name: string) => ` style="color:hsl(${shift(hash(name)) % 360},72%,75%)"`;

const msg = (user: User, text: string, action?: boolean) => {
  const sanitize = (text: string) => text.replace(/[&<>"]/g, c => `&#${c.charCodeAt(0)};`);

  const author = `<span${color(user.nick)}>${user.nick}</span>`;
  if (user.mode !== Mode.Op) text = sanitize(text);

  if (action) {
    return `* ${author} ${text}`;
  }

  return `&lt;${user.mode + author}> ${text}`;
};

export default defineWebSocketHandler({
  open(peer) {
    const nick = new URL(peer.url, "http://localhost").searchParams.get("nick");
    if (nick && /^[\w _]+$/.test(nick)) {
      peer.send(`Welcome back, <span${color(nick)}>${nick}</span>!`);
      users.set(peer.id, { nick, mode: Mode.None });
    } else {
      const nick = `user${(Math.random() * 8999 + 1000) | 0}`;

      peer.send("Welcome! You are currently hidden, send a message (or use /nick) to join the chat!");
      users.set(peer.id, { nick, mode: Mode.None });
    }

    peer.subscribe("chat");
  },
  message(peer, message) {
    const text = message.text();
    const user = users.get(peer.id);

    if (text.startsWith("/")) {
      const [cmd, args] = text.slice(1).split(/ (.*)/);

      if (cmd === "admin") {
        if (args !== ":3") throw "fish";
        user.mode = Mode.Op;
      } else if (cmd === "nick") {
        if (/^[\w _]+$/.test(args)) {
          const message = msg(user, `is now ${args}`, true);
          user.nick = args;
          peer.send(message);
          peer.publish("chat", message);
        } else {
          peer.send(msg(Server, "/nick: invalid nickname"));
        }
      } else if (cmd === "me") {
        const message = msg(user, args, true);
        peer.send(message);
        peer.publish("chat", message);
      } else {
        peer.send(msg(Server, `/${cmd}: unknown command`));
      }
    } else {
      console.log(`<${user.mode + user.nick}> ${text}`);

      const message = msg(user, text);
      peer.send(message);
      peer.publish("chat", message);
    }
  },
  close(peer) {
    users.delete(peer.id);
  },
});
