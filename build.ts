import { render } from "@anguish/spite";
import minifyHtml from "@minify-html/node";
import { readFile, writeFile } from "fs/promises";

const [, , input = "/dev/stdin", output = "/dev/stdout"] = process.argv;

const layout = await readFile("views/layout.html", "utf8");

const rendered = render(await readFile(input, "utf8"), {
  layout,
  components: {},
  minifyDirectives: { compress: { passes: 8, unsafe_comps: true } },
});

const minified = minifyHtml.minify(Buffer.from(rendered), {
  do_not_minify_doctype: true,
  ensure_spec_compliant_unquoted_attribute_values: true,
  keep_spaces_between_attributes: true,
  minify_css: true,
});

writeFile(output, minified);
