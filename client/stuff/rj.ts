// @ts-ignore
const inputImage: HTMLImageElement = image;
// @ts-ignore
const dummySvg: SVGElement = dummy_svg;

// THIS THING WORKS. DONT ASK.

const fontLoad = fetch("/assets/sfpro.woff2").then(d => d.blob()).then(d => {
  const url = URL.createObjectURL(d);
  dummySvg.insertAdjacentHTML(
    "beforeend",
    `<style>@font-face{font-family:SF Pro;font-weight:light;src:url("${url}")}</style>`,
  );
});

var DEFAULT_IMAGE =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIAAQMAAADOtka5AAAABlBMVEW4sbXY0NWg7jAXAAAAnElEQVR4AWKAgf9QQC4f0K4dEgEAACAM7N+aCGi4L/Bier8AAAAAAAAAAAAAAAAAAPEAAAAAAAAAAAAAAAAANEALAAAAAAAAAAAAAAAAAP4DAAAAAAAAAAAAAAAAAP4DAAAAAAAAAAAAAAAAAP4DAAAAAAAAAAAAAAAAAP4DAAAAAAAAAAAAAAAAAP4DAAAAAAAAAAAAAAAAAONAAFx9w7KRsGzoAAAAAElFTkSuQmCC";

var updateUrl = (oldUrl: string, file: File) => {
  URL.revokeObjectURL(oldUrl);
  return URL.createObjectURL(file);
};

var exportImage = () =>
  fontLoad.then(() =>
    new Promise((resolve) => {
      const img = document.createElement("img");
      img.onload = () =>
        // delay for good luck
        setTimeout(() => {
          const canvas = document.createElement("canvas");
          canvas.width = inputImage.width;
          canvas.height = inputImage.height;
          canvas.getContext("2d")!.drawImage(img, 0, 0);
          resolve(canvas.toDataURL());
        }, 200);

      img.src = `data:image/svg+xml,${encodeURIComponent(new XMLSerializer().serializeToString(dummySvg))}`;
    })
  );
